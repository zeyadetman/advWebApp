angular
        .module('ngCribs')
        .controller('cribsController', function ($scope) {
        $scope.cribs = [
            {
                "Type": "Condo",
                "Price": 22000,
                "Address": "22 Grove Street",
                "Description": "Excellent place, Really Excellent"
            },
            {
                "Type": "Bondo",
                "Price": 33000,
                "Address": "33 Grove Street",
                "Description": "Bad place, Really Bad"
            },
            {
                "Type": "Aondo",
                "Price": 11000,
                "Address": "11 Grove Street",
                "Description": "Moderate place, Really Moderate"
            }
            
        ];
    });